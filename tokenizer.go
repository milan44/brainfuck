package main

import (
	"fmt"
	"strconv"
)

type Token struct {
	tokenType string
	value     []Token
	index     int
}

const TypePlus = "+"
const TypeMinus = "-"
const TypeLeft = "<"
const TypeRight = ">"
const TypeInput = ","
const TypeOutput = "."
const TypeLoop = "loop"

func tokenize(source string) []Token {
	tokens, _ := extractTokens(source, 0)
	return tokens
}

func extractTokens(source string, index int) ([]Token, int) {
	var tokens []Token

	for i, char := range source {
		if i < index {
			continue
		}
		index = i

		switch char {
		case '+':
			tokens = append(tokens, Token{
				tokenType: TypePlus,
				index:     i + 1,
			})
		case '-':
			tokens = append(tokens, Token{
				tokenType: TypeMinus,
				index:     i + 1,
			})
		case '<':
			tokens = append(tokens, Token{
				tokenType: TypeLeft,
				index:     i + 1,
			})
		case '>':
			tokens = append(tokens, Token{
				tokenType: TypeRight,
				index:     i + 1,
			})
		case ',':
			tokens = append(tokens, Token{
				tokenType: TypeInput,
				index:     i + 1,
			})
		case '.':
			tokens = append(tokens, Token{
				tokenType: TypeOutput,
				index:     i + 1,
			})
		case '[':
			t, newIndex := extractTokens(source, i+1)
			tokens = append(tokens, Token{
				tokenType: TypeLoop,
				index:     i + 1,
				value:     t,
			})
			index = newIndex
		case ']':
			return tokens, i + 1
		}
	}

	return tokens, index
}

func debugTokens(tokens []Token, ident string) {
	for _, token := range tokens {
		fmt.Println("[" + strconv.Itoa(token.index) + "] " + ident + token.tokenType)

		if token.tokenType == TypeLoop {
			debugTokens(token.value, ident+"  ")
		}
	}
}
